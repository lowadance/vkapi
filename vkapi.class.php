<?php
/*
#VK.API
#Victor Myagkov
#November 2014;
*/

class vkapi{
    private $token;
    private $api_ver = '5.27';
    private $api_server = 'https://api.vk.com/method/';
    
    public function __construct($token) {
        $this->token = $token;
    }

  

  public function parametrs($params) {
	$resp = array();
	foreach($params as $k=>$v) {
	$resp[] = $k.'='.urlencode($v);
	}
	return implode('&',$resp);
	}
        
   public function api($method, $params){
       $params['access_token'] = $this->token;
       $params['v'] = $this->api_ver;
       $send = $this->parametrs($params);
       $result = $this->send($send, $method);  
       return $result;
       
   }
   
   public function send($data, $method){
       $server = $this->api_server.$method.'?'.$data;
       $result = file_get_contents($server);
       return $result;
   }

}